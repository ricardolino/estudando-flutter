import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List _tasks = [];

  TextEditingController _controller = TextEditingController();

  Map<String, dynamic> _lastDeleteTask = Map();

  _getFile() async{
    final Directory directory = await getApplicationDocumentsDirectory();
    return File('${directory.path}/dados.json');
  }

  _saveData(String? t) async{
    var file = await _getFile();

    if (t != null){
      Map<String, dynamic> task = Map();
      task['task'] = t;
      task['active'] = false;
      setState(() {
        _tasks.add(task);
      });
    }

    String data = json.encode(_tasks);
    await file.writeAsString(data);
  }

  _readFile () async{
    try{
      var file = await _getFile();
      return file.readAsString();
    }catch(e){
      return null;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _readFile().then((dados){
      setState(() {
        _tasks = json.decode(dados);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de tarefas'),
        backgroundColor: Colors.purple,
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: _tasks.length,
              itemBuilder: (context, index){
                return Dismissible(
                  background: Container(
                    color: Colors.green,
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(
                          Icons.edit,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                  secondaryBackground: Container(
                    color: Colors.red,
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                  key: Key('${_tasks[index]['task'].toString()}${_tasks[index]['active']}${index.toString()}'), 
                  child: CheckboxListTile(
                    title: Text(_tasks[index]['task']),
                    value: _tasks[index]['active'],
                    onChanged: (bool? valor){
                      setState(() {
                        _tasks[index]['active'] = valor;
                        _saveData(null);
                      });
                    }
                  ),
                  confirmDismiss: (direction) async{
                    if(direction == DismissDirection.endToStart){
                      _lastDeleteTask = _tasks[index];

                      setState(() {
                        _tasks.removeAt(index);
                      });
                      _saveData(null);

                      final snackbar = SnackBar(
                        content: Text("Tarefa removida"),
                        duration: Duration(seconds: 5),
                        action: SnackBarAction(
                          label: "Desfazer",
                          onPressed: (){
                            setState(() {
                              _tasks.insert(index, _lastDeleteTask);
                            });
                            _saveData(null);
                          },
                        ),
                      );
                      ScaffoldMessenger.of(context)
                        .showSnackBar(snackbar);
                      return true;
                    }else{
                      showDialog(
                        context: context, 
                        builder: (context){
                          return AlertDialog(
                        title: Text(
                          'Alterar tarefa',
                          style: TextStyle(color: Colors.purple)
                        ),
                        content: TextField(
                          keyboardType: TextInputType.text,
                          cursorColor: Colors.purple,
                          decoration: InputDecoration(
                            labelText: 'tarefa',
                            labelStyle: TextStyle(color: Colors.purple),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.purple),
                            ),
                          ),
                          style: TextStyle(color: Colors.purple),
                          controller: _controller,
                        ),
                        actions: [
                          TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.purple
                            ),
                            onPressed: (){
                              _controller.text = '';
                              Navigator.pop(context);
                            }, 
                            child: Text(
                              'Cancelar',
                              style: TextStyle(color: Colors.purple),
                            )
                          ),
                          TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.purple
                            ),
                            onPressed: (){
                              setState(() {
                                _tasks[index]['task'] = _controller.text;
                              });
                              _saveData(null);
                              _controller.text = '';
                              Navigator.pop(context);
                            }, 
                            child: Text(
                              'Salvar',
                              style: TextStyle(color: Colors.purple),
                              )
                          )
                        ],
                      );
                        }
                      );
                      return false;
                    }
                  },
                );
             }
            )
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purple,
        child: Icon(Icons.add),
        onPressed: (){
          showDialog(
            context: context, 
            builder: (context){
              return AlertDialog(
                title: Text(
                  'Adicionar tarefa',
                  style: TextStyle(color: Colors.purple)
                ),
                content: TextField(
                  keyboardType: TextInputType.text,
                  cursorColor: Colors.purple,
                  decoration: InputDecoration(
                    labelText: 'tarefa',
                    labelStyle: TextStyle(color: Colors.purple),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.purple)
                    )
                  ),
                  style: TextStyle(color: Colors.purple),
                  controller: _controller,
                ),
                actions: [
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.purple
                    ),
                    onPressed: (){
                      _controller.text = '';
                      Navigator.pop(context);
                    }, 
                    child: Text(
                      'Cancelar',
                      style: TextStyle(color: Colors.purple),
                      )
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.purple
                    ),
                    onPressed: (){
                      _saveData(_controller.text.toString());
                      _controller.text = '';
                      Navigator.pop(context);
                    }, 
                    child: Text(
                      'Salvar',
                      style: TextStyle(color: Colors.purple),
                      )
                  )
                ],
              );
            }
          );
        }
      ),
    );
  }
}