import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _valorBitcoin = 0.0;

  _atualizarValorBitcoin() async{
    Uri url = Uri.parse("https://blockchain.info/ticker");
    http.Response _resposta = await http.get(url);
    Map<String, dynamic> objetoValoresAtuais = json.decode(_resposta.body);
    setState(() {
      _valorBitcoin = objetoValoresAtuais["BRL"]["last"];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(20,0,20,10),
            child: Image.asset("./images/bitcoin.png"),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              "R\$ ${_valorBitcoin.toString()}",
              style: TextStyle(
                fontFamily: 'sans-serife',
                fontSize: 30,
                fontWeight: FontWeight.normal,
                color: Color.fromARGB(255, 95, 95, 97),
                decoration: TextDecoration.none
              ),
              ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: TextButton(
              style: TextButton.styleFrom(
                backgroundColor: Color.fromARGB(255, 255, 161, 9),
              ),
              child: Text("Atualizar"),
              onPressed: _atualizarValorBitcoin,
            ),
          )
        ]
      ),
    );
  }
}