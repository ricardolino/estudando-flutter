import 'package:flutter/material.dart';

void main () => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _escolhaUsuario = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Switch"),
      ),
      body:Container( 
        child: Row(
          children: [
            Switch(
              value: _escolhaUsuario, 
              onChanged: (bool valor){
                setState(() {
                  _escolhaUsuario = valor;
                });
              }
            ),
            /**
             * Switch - Cria widget switch
             *  value - Atributo obrigatório, com resposabilidade de controlar o estado do switch
             *  onChaged - Parâmetro obrigatório, com resposabilidade de controla o evento de mudança
             */
            Text("Deseja ser notificado ?")
          ],
        )
      )
    );
  }
}