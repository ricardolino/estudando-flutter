import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Alinhamento",
    home: Container(
      margin: EdgeInsets.only(top: 40),
      decoration: BoxDecoration(
        border: Border.all(width: 3, color: Colors.black),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        /*mainAxisAlignment - Atributo que controla o alinhamento do eixo principal (main)
            MainAxisAlignment - Paramentro que define o mainAxisAlignment
              .start
              .center
              .end
              .spaceAround
              .spaceBetween
              .spaceEvenly
        */
        crossAxisAlignment: CrossAxisAlignment.start,
        /*crossAxisAlignment - Atributo que controla o alinhamento do eixo que cruza o eixo principal (cross)
            CrossAxisAlignment - Paramentro que define o crossAxisAlignment
              .start
              .end
              .center
              .baseline
              .stretch
              .values
         */
        children: [
          Text("T1"),
          Text("T2"),
          Text("T3")
        ],
      ),
    ),
  ));
}