import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Imagem",
    home: Container(
      margin: EdgeInsets.only(top: 40),
      decoration: BoxDecoration(
        border: Border.all(width: 3, color: Colors.white)
      ),
      child: Image.asset(
        /*Image - widget de importação de imagens
            - É necessário fazer uma préconfiguração no arquivo pubspec.yaml
              flutter:
                uses-material-design: true
                assets:
                  - ./images/mesa.jpg
                  - ./images/parque.jpg
            - .asset("caminho/imagem.jpg") - Forma de importação de imagem

         */
        "./images/mesa.jpg",
        fit: BoxFit.scaleDown, 
        /*fit - Forma de formatação da imagem
            BoxFit - Forma de modificar a formatação
              .none - Tamanho original
              .contain - Default
              .cover - cobrir toda área disponível ocultando partes da imagem
              .fill - cobre toda área disponível sem ocultar partes da imagem
              .fitHeight
              .fitWidth
              .scaleDown - Tamanho proporcional
        */

        //width: 100, - Forma de controle da largura da imagem
        //height: 100, - Forma de controle do tamanho da imagem
      ),
    ),
  ));
}