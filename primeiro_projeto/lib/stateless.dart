import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(), //stateless criado abaixo
  ));
}

class Home extends StatelessWidget { //Classe stateless -> Permite a criação de Widgets que não podem ser alterados
  const Home({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var titulo = "Instagram";

    return Scaffold( //return - Retorna o widget criado
      appBar: AppBar(
        title: Text(titulo),
        backgroundColor: Colors.green,
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Text("Conteúdo principal"),
        ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.lightGreen,
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Row(children: [
            Text("Texto"),
            Text("Texto")
          ]),
        ),
      ),
    );
  }
}