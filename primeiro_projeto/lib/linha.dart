import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Frases do dia - linha",
    home: Row(children: [ //Row - widget para organizar em linhas / children: [] - Sequências de widgtes dentro do widget pai
      Text("T1"), //Text - text widget
      Text("T2"),
      Text("T3")
    ],),
  ));
}