import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {//Classe stateful - Permite a criação de widgets alteraveis
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState(); //createState() - Criou o primeiro estado, o estadio inicial do do app
}

class _HomeState extends State<Home> {
  var texto = "Ricardo"; //Stateful precisa da variável seja declarada acima do build 
  var titulo = "Instagram";

  // void funcao(){
  //   setState(() {//setState(() {}) - Altera o estado do atual, chamando o build e colocando os valores alterados abaixo
  //     texto = "texto alterado";
  //     });
  // };

  void funcao() => setState(() {texto = "texto alterado";});

  @override
  Widget build(BuildContext context) {

    print("Build chamado");

    return Scaffold(
      appBar: AppBar(
        title: Text(titulo),
        backgroundColor: Colors.green,
      ),
      body: Container(
        child: Column(children: [
          TextButton(
            onPressed: funcao,
            child: Text("Clique"),
            ),
          Text("Nome: $texto")
        ],),
      ),
    );
  }
}