import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? _radioEscolhido = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Entrada de dados"),
        backgroundColor: Colors.green,
      ),
      body: Row(
        children: [
          RadioListTile(
            title: Text("Masculino"),
            value: "Masculino", 
            groupValue: _radioEscolhido, 
            onChanged: (String? escolha){
              print(escolha);
              setState(()=>_radioEscolhido = escolha);
            }
          ),
          RadioListTile(
            title: Text("Feminino"),
            value: "Feminino", 
            groupValue: _radioEscolhido, 
            onChanged: (String? escolha){
              print(escolha);
              setState(()=>_radioEscolhido = escolha);
            }
          )
          /**
           * RadioListTile - Widget de criação de botão Radio, permite criar o texto junto
           *  title - Atributo que controla o texto vinculado ao radio
           *  value - Atributo obrigatório que controla o valor que será enviado ao clicar no radio
           *  groupValue - Atributo obrigatório que controla de qual grupo este radio faz parte
           *  onChanged - Atributo obrigatório, evento de mudança de estado
           */
        ]
        ),
    );
  }
}