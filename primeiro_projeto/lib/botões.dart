import 'package:flutter/material.dart';

void botaoPressionado(){
  print("Botão Pressionado");
}

void main(){
  runApp(MaterialApp(
    title: "Botões",
    home: Container(
      color: Colors.white,
      child: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          TextButton( 
            onPressed: botaoPressionado, //onPressed - Atributo obrigatório de FlatButton, executa uma ação após o evento do clique
            child: Text( //child - Atributo obrigatório de FlatButton, Controla o valor do botão, ou seja o que estpa escrito no botão e estilo
              "Clique aqui",
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                decoration: TextDecoration.none
              ),
              )
            )
        ],
      ),
    ),
  ));
}

/*
GestureDestector - Classe que detecta se foi feito alguma interação com o widget filho dele
  - Contém uma série de atributos para detectar gesto.Ex:
  ontap - Clicar em cima
  Exemplo de uso do GestureDestector:
  GestureDestector(
    ontap: (){},
    child: widgetQualquer
  )
 */