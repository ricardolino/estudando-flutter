import 'package:flutter/material.dart';
import 'package:primeiro_projeto/navegacao-basica/telaSecundaria.dart';

class telaPrincipal extends StatefulWidget {
  const telaPrincipal({ Key? key }) : super(key: key);

  @override
  State<telaPrincipal> createState() => _telaPrincipalState();
}

class _telaPrincipalState extends State<telaPrincipal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tela Princiapl"),
        backgroundColor: Colors.orange,
      ),
      body: Container(
        padding: EdgeInsets.all(60),
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: Colors.orange,
            padding: EdgeInsets.all(20)
          ),
          child: Text("Ir para a segunda tela"),
          onPressed: (){
            Navigator.push(
              context, 
              MaterialPageRoute(
                builder: (context) => telaSecundaria()
              )
            );
            /**
             * Navigator - Objeto para fazer a navegação de tela, tem uma série de métodos para fazer a navegação
             *  .push - Abri uma página em cima da atual
             *  context - Constante necessária para fazer a navegação
             *  MaterialPageRoute - Objeto que irá passar a rota da tela secundária
             *    builder - Atributo para criar a tela secundária
             */
          }
        ),
      ),
    );
  }
}