import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool? isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Entrada de dados"),
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: [
          CheckboxListTile(
            title: Text("Título checkbox"),
            subtitle: Text("Subtítulo checkbox"),
            activeColor: Colors.yellow,
            selected: true,
            secondary: Icon(Icons.add_box),
            value: isChecked, 
            onChanged: (bool? valor){
              setState(() {
                isChecked = valor;
              });
            },
          )
          /**
           * CheckboxListTile - Widget para criação de checkbox, permite criar texto junto da caixa
           *  title - Atributo para criação do texto principal cinculado ao checbox
           * subtitle - Atributo para criação do subtítulo
           * activeColor - Atributo que irá colocar uma cor no checkbox quanto ele estiver ativo
           * selected - Atributo que coloca as cores do activeColor no title e subtitle
           * secondary - Atributo que permite a criação de um ícone no checkbox
           *  Icon - Classe para colocar ícones
           *    Icons - Objeto com os ícones
           * value - Atributo obrigatório de controle de uso do checkbox
           * onChanged - Atributo obrigatporio, evento de mudança de estado
           */
        ]
        ),
    );
  }
}