import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List imagens = [
    './images/pedra.png',
    './images/papel.png',
    './images/tesoura.png'
  ];

  String imgemEscolhadoApp = './images/padrao.png';
  String resultado = 'Escolha sua opção';
  var random = Random();

  void jogar(jogada){
    int escolhaApp = random.nextInt(imagens.length);
    imgemEscolhadoApp = imagens[escolhaApp];
    if (jogada == escolhaApp){
      resultado = 'Empate';
    }else if (jogada == 0){
      if (escolhaApp == 1){
        resultado = 'Vocẽ perdeu :(';
      }else{
        resultado = 'Voê ganhou';
      }
    }else if (jogada == 1){
      if (escolhaApp == 2){
        resultado = 'Vocẽ perdeu :(';
      }else{
        resultado = 'Voê ganhou';
      }
    }else if (jogada == 2){
      if (escolhaApp == 0){
        resultado = 'Vocẽ perdeu :(';
      }else{
        resultado = 'Voê ganhou';
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('JokenPo'),
        backgroundColor: Colors.lightBlue,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0,30,0,5),
              child: Text('Escolha do App:')
              ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
              child: Image.asset(
              imgemEscolhadoApp,
              width: 70,
              height: 70)
              ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 10, 0, 5),
              child: Text(resultado)
              ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                    onPressed: (){
                      setState(() {
                        jogar(0);
                      });
                    }, 
                    child: Image.asset(
                      imagens[0],
                      width: 70,
                      height: 70,
                  )),
                  TextButton(
                    onPressed: (){
                      setState(() {
                        jogar(1);
                      });
                    }, 
                    child: Image.asset(
                      imagens[1],
                      width: 70,
                      height: 70,
                  )),
                  TextButton(
                    onPressed: (){
                      setState(() {
                        jogar(2);
                      });
                    }, 
                    child: Image.asset(
                      imagens[2],
                      width: 70,
                      height: 70,
                  ))
            ],))
          ],
        )),
    );
  }
}