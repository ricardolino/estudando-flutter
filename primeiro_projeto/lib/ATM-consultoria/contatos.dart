import 'package:flutter/material.dart';

class telaContatos extends StatelessWidget {
  const telaContatos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Contato",
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Row(
              children: [
                Image.asset("./images/imagens-appATM/detalhe_contato.png"),
                Text("Contatos")
              ],
            ),
            Text("Email: consultoria@atm.com.br"),
            Text("Telefone: (11) 3333-2222"),
            Text("Celular: (11) 99999-8888")
          ]),
        ),
    );
  }
}
