import 'package:flutter/material.dart';

class telaClientes extends StatelessWidget {
  const telaClientes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Clientes",
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Row(
              children: [
                Image.asset("./images/imagens-appATM/detalhe_cliente.png"),
                Text("Clientes")
              ],
            ),
            Image.asset("./images/imagens-appATM/cliente1.png"),
            Text("Empresa de software"),
            Image.asset("./images/imagens-appATM/cliente2.png"),
            Text("Empresa de auditoria")
          ]),
        ),
    );
  }
}
