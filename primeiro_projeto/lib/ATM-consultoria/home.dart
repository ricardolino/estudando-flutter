import 'package:flutter/material.dart';
import 'package:primeiro_projeto/ATM-consultoria/clientes.dart';
import 'package:primeiro_projeto/ATM-consultoria/contatos.dart';
import 'package:primeiro_projeto/ATM-consultoria/empresa.dart';
import 'package:primeiro_projeto/ATM-consultoria/servicos.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "ATM Consultoria",
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(0, 100, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              "./images/imagens-appATM/logo.png",
              height: 65,
            ),
            Container(
              margin: EdgeInsets.fromLTRB(40, 25, 40, 0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(0, 0, 10, 10)
                        ),
                        child: Image.asset(
                          "./images/imagens-appATM/menu_empresa.png",
                          height: 80,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context) => telaEmpresa()
                              )
                          );
                        }
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(10, 0, 0, 10)
                        ),
                        child: Image.asset(
                          "./images/imagens-appATM/menu_servico.png",
                          height: 80,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => telaServicos()
                              )
                            );
                        }
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(0, 10, 10, 0)
                        ),
                        child: Image.asset(
                          "./images/imagens-appATM/menu_cliente.png",
                          height: 80,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context, 
                            MaterialPageRoute(
                              builder: (context) => telaClientes()
                            )
                          );
                        }
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding: EdgeInsets.fromLTRB(10, 10, 0, 0)
                        ),
                        child: Image.asset(
                          "./images/imagens-appATM/menu_contato.png",
                          height: 80,
                        ),
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => telaContatos()
                            )
                          );
                        }
                      )
                    ],
                  ),
                ],
              )
            )
          ]
          ),  
      ),
    );
  }
}

/**
 * GestureDetector() - Objeto que pode detectar eventos, tem uma série de atributos para detectar diferentes eventos
 *  ontap - Evento de toque, recebe funcao (nomefuncao ou (){})
 *  child - Atributo que permitir adcionar filho, imagem, texto...
 */