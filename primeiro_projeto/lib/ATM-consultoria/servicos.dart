import 'package:flutter/material.dart';

class telaServicos extends StatelessWidget {
  const telaServicos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Serviços",
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
        backgroundColor: Colors.green,
      ),
      body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Row(
              children: [
                Image.asset("./images/imagens-appATM/detalhe_servico.png"),
                Text("Nossos Servicos")
              ],
            ),
            Text("Consultoria"),
            Text("Preços"),
            Text("Acompanhamento de projetos")
          ]),
        ),
    );
  }
}
