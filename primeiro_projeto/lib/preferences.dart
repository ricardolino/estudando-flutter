import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));


class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? _textoSalvo = 'Sem valor';
  TextEditingController _controller = TextEditingController();

  _salvar() async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString("nome", _controller.text);
  }

  _recuperar() async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      // if (prefs.getString("nome") == null){
      //   _textoSalvo = 'Sem valor';
      // }else{
      //   _textoSalvo = prefs.getString("nome");
      // }
      _textoSalvo = prefs.getString("nome") ?? 'Sem valor';
    });
  }

  _remover() async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove("nome");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Preferences'),
      ),
      body: Center(
        child: Column(
          children: [
            Text(_textoSalvo.toString()),
            TextField(
              controller: _controller,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Digite algo"
              )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  child: Text(
                    'Salvar',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue
                  ),
                  onPressed: _salvar,
                ),
                TextButton(
                  child: Text(
                    'Recuperar',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue
                  ),
                  onPressed: _recuperar,
                ),
                TextButton(
                  child: Text(
                    'Remover',
                    style: TextStyle(
                      color: Colors.white
                    ),
                  ),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blue
                  ),
                  onPressed: _remover,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}