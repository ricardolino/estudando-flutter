import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class PlayVideo extends StatefulWidget {
  PlayVideo({Key? key, required this.idVideo}) : super(key: key);
  final String idVideo;

  @override
  State<PlayVideo> createState() => _PlayVideoState();
}

class _PlayVideoState extends State<PlayVideo> {
  late YoutubePlayerController _controller;

  @override
  void initState(){ //Primeiro método chamado dentro de um StatefulWidget
    super.initState();
    _controller = YoutubePlayerController(
      initialVideoId: widget.idVideo,
      flags: YoutubePlayerFlags(
        autoPlay: true,
      ),
    );
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void didChangeDependencies() { //Segundo método chamado dentro de um StatefulWidget
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant PlayVideo oldWidget) { //Segundo método chamado dentro de um StatefulWidget, depedendo do contexto. Caso haja alguma mudança dentro da mesma tela
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() { //Quarto método chamado dentro de um StatefulWidget, método chamado quando finaliza a sessão dentro da página
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) { //Terceiro método chamado dentro de um StatefulWidget
    return YoutubePlayerBuilder(
      player: YoutubePlayer(
        controller: _controller,
      ),
      builder: (context, player){
        return Scaffold(
          appBar: AppBar(
            title: Image.asset(
              "./images/youtube.png",
              width: 75,
            ),
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.grey) //Altera a cor do tema, independente se o icon já foi herdado ou criado na página
          ),
          body: Column(
            children: [
              player,
            ],
          ),
        );
      }
    );
  }
}

/**
 * import 'package:youtube_player_flutter/youtube_player_flutter.dart'; - Necessário declarar "youtube_player_flutter: ^8.1.0" em pubspec.yaml e minSdkVersion 17 (android-app-build.gradle)
 * late YoutubePlayerController _controller; - Declarar controlador, late é utlizado para mostrar que será usado depois, YoutubePlayerController tipo de dado do controlador
 *  YoutubePlayerController() - Widget de de controle do vídeo
 *    initialVideoId - Atributo que recebe o id do vídeo
 *    flags - Atributo que controla o estilo do vídeo
 *      YoutubePlayerFlags() - Controle de caracteristicas do vídeo
 *        autoPlay - Recebe um booleano para controla se o vídeo vai rodar automático ao iniciar tela
 * YoutubePlayerBuilder() - Atributo que envelopa todo o widget principal da página, e permite mostrar o vídeo usando o "player"
 *  player - Atributo utilizado para declerar o controlador
 *    YoutubePlayer() - Classe que será declarado o controlador
 *      controller - Atributo que é declerado o controlador
 *  builder(context, player){} - Retorna uma widget do corpo da página, necessário parâmetro context e player
 *    player - O vídeo em si.
 */