import 'package:flutter/material.dart';
import 'package:primeiro_projeto/youtube/Api.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/Inicio/playVideos.dart';
import 'package:primeiro_projeto/youtube/models/video.dart';

class Inicio extends StatefulWidget {
  final String pesquisa;
  const Inicio({ Key? key, required this.pesquisa}) : super(key: key);

  @override
  State<Inicio> createState() => _InicioState();
}

class _InicioState extends State<Inicio> {

  Future<List<Video>> _loadVideos(String pesquisa) async{
    print('Pesquisa: '+ pesquisa);
    Api api = Api();
    return await api.pesquisar(pesquisa); //Necessário o await para esperar a api carregar os dados, para user o await é necessário usar o async na função
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Video>>(
      future: _loadVideos(widget.pesquisa),
      builder: (context, snapshot){
        switch(snapshot.connectionState){
          case ConnectionState.none:
          case ConnectionState.waiting:
            print('esperando');
            Center(child: CircularProgressIndicator());
            break;
          case ConnectionState.active:
          case ConnectionState.done:
            if(snapshot.hasData){
              print('Tem dados');
              return ListView.separated( //ListView.separated - ListView que permite criar um separador
                itemCount: snapshot.data!.length,
                separatorBuilder: (context, index) => Divider(
                  height: 5,
                  color: Colors.grey,
                  thickness: 5,
                  /**separatorBuilder - Separador do itens, o filho deve ser uma função com os parâmetros context e index
                   *  Divider - Criar um divisor
                   *    height - Altura
                   *    color - cor
                   *    thickness - Grossura
                   */
                ),
                itemBuilder: (context, index){
                  // print(snapshot.data![index].image.toString());
                  // return Column(
                  //   children: [
                  //     Image.network(
	                //       snapshot.data![index].image.toString(),
                  //       fit: BoxFit.cover,
                  //     ),
                  //     Text(snapshot.data![index].title.toString())
                  //   ],
                  // );
                  return GestureDetector(
                    onTap: (){
                      // print(snapshot.data![index].id.toString());
                      Navigator.push(
                        context, 
                        MaterialPageRoute(
                          builder: (context) => PlayVideo(idVideo: snapshot.data![index].id.toString())
                        ) 
                      );
                    },
                    child: Column(
                      children: [
                        Container(
                          height: 200,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(snapshot.data![index].image.toString()),
                              fit: BoxFit.cover
                              /**image - Permite colocar uma imagem de decoração, porém é necessário o objeto DecorationImage
                              *  DecorationImage - Objeto para decoração de imagens no container
                              *    image - Atributo que permite adicionar imagens
                              *      NetworkImage - Passando a url em string acessa imagens na internet.
                              */
                            )
                          ),
                        ),
                        ListTile(
                          title: Text(snapshot.data![index].title.toString()),
                        )
                      ],
                    ),
                  );
                }
              );
            }else{
              Center(child: CircularProgressIndicator());
            }
            break;
        }
        return Center(child: CircularProgressIndicator());
      }
    );
  }
}