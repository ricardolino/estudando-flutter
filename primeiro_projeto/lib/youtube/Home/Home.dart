import 'package:flutter/material.dart';
import 'package:primeiro_projeto/youtube/Home/CustomSearchDelegate.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/Biblioteca.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/EmAlta.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/Inicio/Inicio.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/Inscricao.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _indice = 0;
  String _pesquisaDigitada = "";

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetsBody = [
      Inicio(pesquisa: _pesquisaDigitada),
      EmAlta(),
      Inscricao(),
      Biblioteca()
    ];

    return Scaffold(
      appBar: AppBar(
        title: Image.asset(
          "./images/youtube.png",
          width: 60,
        ),
        actions: [
          // IconButton(
          //   onPressed: (){},
          //   icon: Icon(Icons.videocam,color: Colors.grey,),
          // ),
          Builder(
            builder: (context) => IconButton( 
              onPressed: () {
                Scaffold.of(context).setState(() {
                  showSearch(context: context, delegate: CustomSearchDelegate());
                });
              },
              icon: Icon(Icons.search, color: Colors.grey),
            ),
          )
          /**
           * Builder - Widget builder, utlizado para criar um novo contexto (context), utilizado para usar o Scaffold.of(context).
           *  builder - Atributo para criar novo build, necessário o parâmetro context
           * 
           * Scaffold.of(context). - Utilizado para envelopar um contexto específico, neste caso utizado para criar um campo Search
           *  .setState(() {}); - Método de mudança de estado
           * 
           * showSearch - Classe com o objetivo de criar um campo de pesquisa, necessário o context e delegate
           *  delegate - Atributo que recebe uma classe herdeira de SearchDelegate, que controla as ações tomadas no search
           */
          // IconButton(
          //   onPressed: (){},
          //   icon: Icon(Icons.account_circle, color: Colors.grey),
          // )
          /**
           * IconButton - Widget de botões com Icons, os atributos onPressed e icon são obrigatórios
           */
        ],
        backgroundColor: Colors.white,
      ),
      body: widgetsBody[_indice],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _indice,
        onTap: (indice) {
          setState(() {
            _indice = indice;
          });
        },
        type: BottomNavigationBarType.fixed, //BottomNavigationBarType.shifting
        fixedColor: Colors.red,
        items: [
          BottomNavigationBarItem(
              //backgroundColor: Colors.orange,
              label: "Início",
              icon: Icon(Icons.home)),
          BottomNavigationBarItem(
              //backgroundColor: Colors.red,
              label: "Em alta",
              icon: Icon(Icons.whatshot)),
          BottomNavigationBarItem(
              //backgroundColor: Colors.blue,
              label: "Inscrições",
              icon: Icon(Icons.subscriptions)),
          BottomNavigationBarItem(
              //backgroundColor: Colors.green,
              label: "Biblioteca",
              icon: Icon(Icons.folder)),
        ],
      ),
      /**
       * bottomNavigationBar - Atributo de scafold que cria um navegation bar no final da tela
       *  BottomNavigationBar - Classe para manipula o navegation bar
       *    currentIndex -Atributo que controla em que indice o navegation bar vai está, por consequência controla em que tela ele estará
       *    onTap - Atributo que controla o evento de toque, ter um atributo indice criado de forma obrigatória, este atributo irá conter o valor do indice de acordo com o que ele for mudado
       *    type - Atributo que controla o tipo de navegation bar
       *      c
       *        .fixed - Terá uma cor padrão determinada no atributo fixedColor
       *        .shifting - Permite que mude a cor de acordo com o clique no item, a cor é determinada na classe BottomNavigationBarItem
       *    fixedColor - Atributo que determina a cor dos objetos quando é usado o BottomNavigationBarType.fixed
       *    items - Atributo que recebe os itens do navagation bar
       *      BottomNavigationBarItem - Widget de criação do intes
       *        backgroundColor - Atributo que determina a cor do navegation bar quando aquele widget está selecionado, porém deter o valor backgroundColor.shifting no atributo type
       *        label - Atributo que recebe uma string para definir o título do item
       *        icon - Atributo que recebe um icon do item
       */
    );
  }
}
