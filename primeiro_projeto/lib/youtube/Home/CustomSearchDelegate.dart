import 'package:flutter/material.dart';
import 'package:primeiro_projeto/youtube/Home/containersHome/Inicio/Inicio.dart';

class CustomSearchDelegate extends SearchDelegate<String> {

  /**
   * SearchDelegate<tipoRetorno> - Classe pai para criar o delegate
   */

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }
  /**
   * buildActions - Método que retorna uma lista de ações que podem ser oganizadas da forma que preferir
   * query - Controla, recebe, alterar o que está no campo de texto
   */

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, "");
      },
    );
  }
  /**
   * buildLeading - Método que controla a ação de voltar para tela anterior, retorna um widget
   * close(context, "") - classe que determina que fecha o search e retorna dados
   */

  @override
  Widget buildResults(BuildContext context) {
    //print("resultado: pesquisa realizada");
    print(query);
    // close(context, query );
    return Inicio(pesquisa: query);
    // return Container();
  }
  /**
   * buildResults - Método que controla o resultado, retorna o widget que aparece ao encontrar resultado
   */

  @override
  Widget buildSuggestions(BuildContext context) {
    //print("resultado: digitado " + query );

    return Container();
    /*
    List<String> lista = List();
    
    if( query.isNotEmpty ){
      
      lista = [
        "Android", "Android navegação", "IOS", "Jogos"
      ].where(
          (texto) => texto.toLowerCase().startsWith( query.toLowerCase() )
      ).toList();

      return ListView.builder(
          itemCount: lista.length,
          itemBuilder: (context, index){

            return ListTile(
              onTap: (){
                close(context, lista[index] );
              },
              title: Text( lista[index] ),
            );

          }
      );
      
    }else{
      return Center(
        child: Text("Nenhum resultado para a pesquisa!"),
      );
    }
    
    */

    /**
     * buildSuggestions - Controla as sugestões
     */
    
  }



}