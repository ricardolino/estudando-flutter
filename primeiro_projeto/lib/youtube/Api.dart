import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:primeiro_projeto/youtube/models/video.dart';

const KEY_API = 'AIzaSyA2NGCHhVlyNRPqzVVDL0YCOuXHEdpQmLw'; //chave da api criado no https://console.cloud.google.com/apis/dashboard
const CHANEL_ID = 'UC0zqi2PTcVaI_9ZtDtnzO8w'; //id do canal (Canal do Antoni Curti) 
const URL_BASE = 'https://www.googleapis.com/youtube/v3/';  //url base da api do youtube


class Api{
  pesquisar(String pesquisa) async{
    // print('Pesquisa api: ' + pesquisa);
    // print(URL_BASE + 'search' + '?part=snippet&type=video&maxResults=20&order=date&key=${KEY_API}&channelId=${CHANEL_ID}&q=${pesquisa}');
    Uri url = Uri.parse(URL_BASE + 'search' + '?part=snippet&type=video&maxResults=20&order=date&key=${KEY_API}&channelId=${CHANEL_ID}&q=${pesquisa}');
    /**
     * ? - Separa a url do parâmentros;
     * & - Separa um parâmetro do outro
     * Sintaxe: 
     *  - parâmetro=valor
     */
    http.Response response = await http.get(url);

    if (response.statusCode == 200){
      Map<String, dynamic> dadosJson = json.decode(response.body);
      List<Video> videos = dadosJson['items'].map<Video>(
        (map){
          return Video.fromJson(map);
        }
      ).toList();

      // for (var video in videos){
      //   print(video.title);
      // }
      print(videos.length);
      return videos;
    }else{
      print("Erro ao fazer pesquisa");
    }
  }
}
