import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List itens = [];
   _carregarItens(){
    for (int i=1; i<11; i++){
      Map<String, String> item = Map();
      item["titulo"] = "Título ${i}";
      item["subtitulo"] = "Subtitulo ${i}";
      itens.add(item);
    }
  }
  @override
  Widget build(BuildContext context) {
    _carregarItens();

    return Scaffold(
      appBar: AppBar(
        title: Text("ListView")
      ),
      body: Container(
        child: ListView.builder(
          itemCount: itens.length,
          itemBuilder: (context, indice){
            return ListTile(
              title: Text(itens[indice]["titulo"]),
              subtitle: Text(itens[indice]["subtitulo"]),
              onTap: (){
                showDialog(
                  context: context,
                  builder: (context){
                    return AlertDialog(
                      title: Text(itens[indice]["titulo"]),
                      content: Text(itens[indice]["subtitulo"]),
                      actions: [
                        TextButton(
                          onPressed: (){
                            print("SIM selecionado");
                            Navigator.pop(context);
                          }, 
                          child: Text("Sim")
                        ),
                        TextButton(
                          onPressed: (){
                            print("NÃO selecionado");
                            Navigator.pop(context);
                          }, 
                          child: Text("Não")
                        )
                      ],
                    );
                  }
                );
                /**
                 * showDialog - Widget para criação de telas sobre
                 *  AlertDialog - widget para criação de tela alert
                 */
              },
            );
          }
        ),
      ),
    );
  }
}