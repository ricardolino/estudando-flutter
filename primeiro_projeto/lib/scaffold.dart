import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold( //Scaffold - Widget que personalizado, para construção de app 
      appBar: AppBar(//appBar - Atributo para a criação do header do app
      //AppBar() - Classe para criação do header
        title: Text("Instagram"),
        backgroundColor: Colors.green, //backgroundColor - Atributo de controle do background
      ),
      body: Padding( //body - Atributo criação do corpo principal 
        padding: EdgeInsets.all(16),
        child: Text("Conteúdo principal"),
        ),
      bottomNavigationBar: BottomAppBar(//bottomNavigationBar - Atributo de criação do footer
      //BottomAppBar() - Classe para criação do footer
        color: Colors.lightGreen,
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Row(children: [
            Text("Texto"),
            Text("Texto")
          ]),
        ),
      ),
    ),
  ));
}