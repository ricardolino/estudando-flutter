import 'package:flutter/material.dart';

void main (){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home:Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String resposta = '';
  TextEditingController controladorAlcool = TextEditingController();
  TextEditingController controladorGasolina = TextEditingController();

  void calcular(calculo){ //não havia necessidade de passar os controladores pela função do butoon, já que estão declaradas junto da função
    setState(() {
      if (calculo < 0.7){
        resposta = 'Utilize Alcool';
      }else{
        resposta = 'Utilize Gasolina';
      }
    });
    controladorAlcool.text = "";
    controladorGasolina.text = "";
    /*Os controladores não precisam estar no setState para fazer a mudança */
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Álcool ou Gasolina'),
        backgroundColor: Colors.blueAccent,
      ),
      body: Center(
        child: SingleChildScrollView( //SingleChildScrollView - Widget para colocar scroll na tela
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,//Utilizando stretch para esticar o botão
            children: [
              Padding(
                padding: EdgeInsets.all(15),
                child: Image.asset('./images/logo.png'),
              ),

              Padding(
                padding: EdgeInsets.all(15),
                child: Text(
                  'Saiba qual a melhor opção para abastaceminto do seu carro',
                  style: TextStyle(
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),

              Padding(
                padding: EdgeInsets.all(15),
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Preço Alcool, ex: 1.59'
                  ),
                  controller: controladorAlcool,
                ),
              ),

              Padding(
                padding: EdgeInsets.all(15),
                child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Preço Gasolina, ex: 3.15'
                  ),
                  controller: controladorGasolina,
                ),
              ),

              Padding(
                padding: EdgeInsets.all(15),
                child: TextButton(
                  onPressed: () => calcular(num.parse(controladorAlcool.text) / num.parse(controladorGasolina.text)),//Classe num método parse para transforma tipo de controlador em numérico, a classe double tem mais opções
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.blueAccent
                  ),
                  child: Text(
                    'Caulcular',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                )
              ),

              Padding(
                padding: EdgeInsets.all(15),
                child: Text(resposta),
              )
            ],
          ) ,
        )
      ),
    );
  }
}