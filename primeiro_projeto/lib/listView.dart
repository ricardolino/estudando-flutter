import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List itens = [];
   _carregarItens(){
    for (int i=1; i<11; i++){
      Map<String, String> item = Map();
      item["titulo"] = "Título ${i}";
      item["subtitulo"] = "Subtitulo ${i}";
      itens.add(item);
    }
  }
  @override
  Widget build(BuildContext context) {
    _carregarItens();

    return Scaffold(
      appBar: AppBar(
        title: Text("ListView")
      ),
      body: Container(
        child: ListView.builder(
          itemCount: itens.length,
          itemBuilder: (context, indice){
            return ListTile(
              title: Text(itens[indice]["titulo"]),
              subtitle: Text(itens[indice]["subtitulo"])
            );
          }
        ),
        /**
         * ListView - Widget de criação de listas, possui vários construtores para fazer as listas
         *  .builder - Um dos contrutores de ListView
         *    itemCount - Atributo que determina o tamanho da lista
         *    itemBuilder - Atributo que recebe a função que irá retorna cada item da lista, precisa receber o parâmetro context e declarar um indice
         *      ListTile - Widget que constroi cada item da lista
         *        title - Título
         *        subtitle - subtitulo
         */
      ),
    );
  }
}