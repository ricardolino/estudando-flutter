import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Formatação de texto",
    home: Container(
      color: Colors.white,
      child: Column( 
        children: [ //child - Atributo filho do container, somente aceita um filho
          Text(
            "Lorem Ipsum is simply dummy text of \nthe",
            style: TextStyle( /*
            * style - atributo de Text que controla a formatação do texto
            * TextStyle - Função para aplicar as formatações
            */
              fontSize: 35,
              fontStyle: FontStyle.normal, //fontStyle - Controla o estilo da fonte (italic e normal)
              fontWeight: FontWeight.normal, //fontWeight - controla a espesura da fonte (negrito)
              letterSpacing: 0, //letterSpacing - Espaçamento entre as letras, recebe um valor numérico
              wordSpacing: 10, //wordSpacing - Espaçamento entre as palavras, recebe um valor numérico
              decoration: TextDecoration.lineThrough, //decoration - Decoração dos textos
              decorationColor: Colors.greenAccent, //decorationColor - Cor do decoration
              decorationStyle: TextDecorationStyle.solid, //decorationStyle - Controla o style do texto
              color: Colors.black //color - Cor do texto
            )
            ),
          Text("Lorem Ipsum is simply dummy text of the"),
          Text("Lorem Ipsum is simply dummy text of the"),
          ],
        ),
      ),
  ));
}