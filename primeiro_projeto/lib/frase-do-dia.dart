import 'package:flutter/material.dart';
import 'dart:math';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  var indice = 0;
  var numeroAletorio = Random();

  List<String> listaFrases= [
    "Vencedor é aquele que luta.",
    "Você tem que seguir em frente.",
    "Não tenha medo da mudança.",
    "Tudo é possível.",
    "No fim tudo dá certo.",
    ];

  void mudarFrase() => setState(() {
    indice = numeroAletorio.nextInt(listaFrases.length);
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Frases do dia"),
        backgroundColor: Colors.green,
      ),
      body: Center( //Widget container que deixa centralizado o seu conteúdo
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("./images/logo.png"),
            Padding(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Text(
                listaFrases[indice],
                style: TextStyle(fontSize: 25),
                )
            ),
            TextButton(
                onPressed: mudarFrase,
                child: Text(
                  "Mudar frase",
                  style: TextStyle(color: Colors.white),
                  ),
                style: TextButton.styleFrom(
                  backgroundColor: Colors.green
                )
            )
          ],
        ),
        ),
    );
  }
}