import 'package:flutter/material.dart';

void main () => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _escolhaUsuario = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Switch"),
      ),
      body:Container( 
        child: Column(
          children: [
            SwitchListTile(
              title: Text("Deseja ser notificado ?"),
              value: _escolhaUsuario, 
              onChanged: (bool valor){
                setState(() {
                  _escolhaUsuario = valor;
                });
              }
            ),
            /**
             * SwitchListTile - Widget de criação do switch com texto
             * Atributos parecidos com o do checkboxList
             */
          ],
        )
      )
    );
  }
}