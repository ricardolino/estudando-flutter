import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  double valor = 5;
  String label = "5";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slider"),
      ),
      body: Container(
        child: Column(children: [
          Slider(
            value: valor, 
            min: 0,
            max: 10,
            label: label,
            divisions: 10,
            activeColor: Colors.red,
            inactiveColor: Colors.orange,
            onChanged: (double novoValor){
              setState(() {
                valor = novoValor;
                label = novoValor.toString();
              });
              print(valor);
            }
          )
          /**
           * Slider - Widget de criação de slider
           *  value - Atributo obrigatório, de controle de posição do slider
           *  min - Atributo que define o valor mínimo do slider
           *  max - Atributo que define o valor máximo do slider
           *  label - Atributo que cria uma mensagem sobre o slider ao movimenta-lo, necessita do atributo divisions para funcionar
           *  divisions - Atributo que divide o slider em uma quantidade fixa
           *  activeColor - Atributo que controla cor da regua que se movimenta no slider
           *  inactiveColor - Atributo que controla cor da regua que não se movimenta
           *  onChanged - Atributo obrigatório, controla o evento de mudança de estado
           */
        ])
      ),
    );
  }
}