import 'package:flutter/material.dart';
import 'package:primeiro_projeto/navegacao-passando-dados/telaPrincipal.dart';

void main() {
  return runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: telaPrincipal(),
  ));
}