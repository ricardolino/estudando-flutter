import 'package:flutter/material.dart';

class Dados {
  final String valor;
  const Dados (this.valor);
}

class telaSecundaria extends StatefulWidget {
    /**
  //Primeiro forma de transmissão dados para segunda tela
  String? dado;
  telaSecundaria({this.dado});
   * Construtor da classe para receber dados de outra tela e aplica-los
   */

  const telaSecundaria({ Key? key, required this.dado}) : super(key: key);
  final Dados dado;

  @override
  State<telaSecundaria> createState() => _telaSecundariaState();
}

class _telaSecundariaState extends State<telaSecundaria> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tela Secundária"),
      ),
      body: Padding(
        padding: EdgeInsets.all(30),
        /**
         * Primeira forma de transmissão de dados
         * child: Text("Tela secundária! Valor passado: ${widget.dado}")
         */
        child: Text("Tela secundária! Valor passado: ${widget.dado.valor}"),
        //É necessário usar o objeto widget para chamar o atributo criado, não necessita do this.
      ),
    );
  }
}