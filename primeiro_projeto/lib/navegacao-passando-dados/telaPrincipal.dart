import 'package:flutter/material.dart';
import 'package:primeiro_projeto/navegacao-passando-dados/telaSecundaria.dart';


class telaPrincipal extends StatefulWidget {
  const telaPrincipal({ Key? key }) : super(key: key);

  @override
  State<telaPrincipal> createState() => _telaPrincipalState();
}

class _telaPrincipalState extends State<telaPrincipal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tela Princiapl"),
        backgroundColor: Colors.orange,
      ),
      body: Container(
        padding: EdgeInsets.all(60),
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: Colors.white,
            padding: EdgeInsets.all(20)
          ),
          child: Text("Ir para a segunda tela"),
          onPressed: (){
            Navigator.push(
              context, 
              MaterialPageRoute(
                //Para transmitir dados é necessário criar o contrutor na tela que receberá os dados
                /**
                 * Primeira forma de transmissão de dados 
                 * builder: (context) => telaSecundaria(dado: 'Ricardo')
                 */
                builder: (context) => telaSecundaria(dado: Dados('Ricardo'))
              )
            );
          }
        ),
      ),
    );
  }
}