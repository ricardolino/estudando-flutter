import 'package:flutter/material.dart';
import 'package:primeiro_projeto/navegacao-declarando-rotas/telaPrincipal.dart';
import 'package:primeiro_projeto/navegacao-declarando-rotas/telaSecundaria.dart';

void main() {
  return runApp(MaterialApp(
    initialRoute: "/",
    routes: {
      "/princiapal":(context) => telaPrincipal(),
      "/secundaria":(context) => telaSecundaria()
    },
    /**
     * Declarando rotas
     * initialRoute - Atributo para declarar qual arquivo será carregado primeiro. Utilize o / para ele iniciar o main.
     * routes - Atributo em que as rotas serão declaradas.
     */
    debugShowCheckedModeBanner: false,
    home: telaPrincipal(),
));
}