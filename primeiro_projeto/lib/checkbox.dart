import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool? ischecked = false; //? Transforma  tipo de variável nullable, ou seja, pode vir a ser null
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Entrada de dados"),
        backgroundColor: Colors.blueAccent,
      ),
      body: Column(
        children: [
          Text("Checkbox"),
          Checkbox(
            value: ischecked,
            onChanged: (bool? valor){ //necessário está com o ? para que se torne nullable
              setState(() {
                ischecked = valor;
              });
            }
          )
          /*
          Checkbox - Widget de criação da caixa de checkbox, somente a caixa e não o texto
            - value - Atributo obrigatório de controle caso o checkbox esteja marcado (true) ou não (false)
            - onChanged - Atributo obrigatório, controla o evento de mudança de estado do checbox
           */
        ]
        ),
    );
  }
}