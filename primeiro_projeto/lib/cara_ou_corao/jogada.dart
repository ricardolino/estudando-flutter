import 'package:flutter/material.dart';
import 'package:primeiro_projeto/cara_ou_corao/home.dart';

class DadosRecebidos{
  final int index;
  const DadosRecebidos(this.index);
}

class Jogada extends StatefulWidget {
  const Jogada({ Key? key , required this.dadoRecebido}) : super(key: key);
  final DadosRecebidos dadoRecebido;

  @override
  State<Jogada> createState() => _JogadaState();
}

class _JogadaState extends State<Jogada> {

  List moedas = [
    "./images/cara_ou_coroa/moeda_cara.png",
    "./images/cara_ou_coroa/moeda_coroa.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      color: Color.fromARGB(255, 100, 186, 144),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(7.5),
            child: Image.asset(moedas[widget.dadoRecebido.index],)
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: GestureDetector(
              child: Image.asset("./images/cara_ou_coroa/botao_voltar.png"),
              onTap: (){
                Navigator.pop(context); //Apaga a tela atual
              },
              ),
          )
        ],
      ),
    );
  }
}