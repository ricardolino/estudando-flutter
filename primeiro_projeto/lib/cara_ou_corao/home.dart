import 'package:flutter/material.dart';
import 'dart:math';
import 'package:primeiro_projeto/cara_ou_corao/jogada.dart';

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  var random = Random();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      color: Color.fromARGB(255, 100, 186, 144),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(7.5),
            child: Image.asset("./images/cara_ou_coroa/logo.png",)
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: GestureDetector(
              child: Image.asset("./images/cara_ou_coroa/botao_jogar.png"),
              onTap: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context) => Jogada(dadoRecebido: DadosRecebidos(random.nextInt(2)))
                  )
                );
              },
              ),
          )
        ],
      ),
    );
  }
}