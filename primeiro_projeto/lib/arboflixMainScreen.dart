import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Base(),
));

class Base extends StatefulWidget {
  const Base({ Key? key }) : super(key: key);

  @override
  State<Base> createState() => _BaseState();
}

class _BaseState extends State<Base> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Text('ARBO'),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)
            ),
              gradient: LinearGradient(
                colors: [Colors.green,Colors.green],
                begin: Alignment.bottomCenter,
                end: Alignment.topCenter
              )
          ),
        ),
        actions: [
          Icon(Icons.search)
        ],
      ),
      drawer: Drawer(),
      body: Container(
        child: Column(),
      ),
    );
  }
}