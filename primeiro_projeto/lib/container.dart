import 'package:flutter/material.dart';

void main (){
  runApp(MaterialApp( //runApp - Função do dart para contruir o app / MaterialApp - Classe no pacote package:flutter/material.dart para contruir o app
    title: "Frases do dia", //title - título do app, não aparece na tela
    home: Container(color: Colors.green), /* 
    home - Tela corpo da página
      Container - Flutter widget (basic), sempre tem o tamanho de acordo com o seu filho, a não ser que use os  parãmetrso de controle de tamanho (widht e height)
        color - atributo que determina a cor / Colors - Método para determinar cores
        */
  ));
}