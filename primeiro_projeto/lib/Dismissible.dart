import 'package:flutter/material.dart';

void main () => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
  home: Home(),
));


class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List _itens = [
    'teste1',
    'teste2',
    'teste3',
    'teste4'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dismissible'),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: _itens.length,
                itemBuilder: (context, index){

                  final item = _itens[index];

                  return Dismissible(
                    background: Container(
                      color: Colors.green,
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.edit,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    secondaryBackground: Container(
                      color: Colors.red,
                      padding: EdgeInsets.all(16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(
                            Icons.delete,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    //direction: DismissDirection.horizontal, - Padrão é .horizontal
                    onDismissed: (direction){

                      if( direction == DismissDirection.endToStart ){
                        print("direcao: endToStart " );
                      }else if( direction == DismissDirection.startToEnd ){
                        print("direcao: startToEnd " );
                      }

                      setState(() {
                        _itens.removeAt(index);
                      });


                    },
                    key: Key( item ),
                    child: ListTile(
                      title: Text( item ),
                    )
                  );
                  /**
                   * Dismissible - Widget para criar animação de remoao de itens dentro de uma lista
                   *  background - Recebe um widget para criar o background no momento da animação
                   *  secondaryBackground - Recebe um widget para criar o background no momento da animação, caso seja uma animação para duas direções
                   *  direction - Junto do DismissDirection. controla a direção da animação, por padrão é horizontal
                   *  onDismissed - É uma função (direction){}, que obrigatoriamente recebe a direção que foi feita a animação e permite fazer manupulações
                   *  key - Atributo obrigatório, recebe a classe Key e cria uma chave, está chave deve ser única para cada item
                   *  child - Atributo obrigatório, onde são colocados os itens da lista
                   */
                }
            ),
          )
        ],
      )
    );
  }
}