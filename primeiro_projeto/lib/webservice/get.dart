import 'package:flutter/material.dart';
import 'package:http/http.dart' as http; //pacote para fazer as requisiçoes http
import 'dart:convert'; //pacote para fazera converção de string-json para objeto

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  TextEditingController _controlador = TextEditingController();
  String _endereco = "";

  _recuperarCep(cep) async {
    Uri url = Uri.parse( "https://viacep.com.br/ws/${cep.text}/json/");
    http.Response _resposta;
    _resposta = await http.get(url);
    /**
     * async - Palavra chave utlizado para determinar que será uma comunicação assíncrona
     * Uri - tipo de dado necessáiro para enviar as requisições
     *  .parse - Transforma em uri
     * http - Pacote para fazer as requisições, necessário importar e publica-lo no arquivo pubspec.yaml
     *  .Response - Objeto responsável pela resposta
     *  .get - Método para fazer requisição get
     * await - Palavra chave para esperar até que seja finalizado a operação http
     * 
     */

    Map<String, dynamic> _enderecoObjeto = json.decode(_resposta.body);
    /**
     * _resposta.body - Retorna uma string do json obtido
     * json - Objeto para tratar dados organizados em json, necessáiro importar pacote
     *  .decode - Transforma uma string json em objeto
     */

    setState(() {
      _endereco = "UF: ${_enderecoObjeto['uf']} / Logradouro: ${_enderecoObjeto['logradouro']} / ddd: ${_enderecoObjeto['ddd']}";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Recuperar endereço")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(10),
              child: TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: "01001000"),
                maxLength: 8,
                controller: _controlador,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: TextButton(
                child: Text("Pesquisar"),
                onPressed: () => _recuperarCep(_controlador),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(_endereco),
            )
          ]
          ),
        ),
    );
  }
}