import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';


void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Post{
  int _userId;
  int _id;
  String _title;
  String _body;

  Post(this._userId, this._id, this._title, this._body);

  // int get userId => _userId;
  int get id => _id;
  String get title => _title;
  // String get body => _body;
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  String _urlBase = "https://jsonplaceholder.typicode.com";

  Future<List<Post>> _recuperarPostagens() async {
    http.Response _dados = await http.get(Uri.parse(_urlBase + "/posts"));

    var postsJson = json.decode(_dados.body);
    List<Post> listPosts = [];

    for (var post in postsJson){
      listPosts.add(Post(post['userId'], post['id'], post['title'], post['body']));
    }

    return listPosts;
  }

  _post () async {
    var bodyPost = json.encode({
      "title": "titulo",
      "body": "corpo",
      "userId": 1,
    });

    /**
     * json
     *  - .encode(objeto) - Transforma objeto em stringJson
     */

    http.Response _response = await http.post(
      Uri.parse(_urlBase + "/posts"),
      headers: {'Content-type': 'application/json; charset=UTF-8',},
      body: bodyPost
    );

  /**
   * http
   *  - .post() - Método para fazer o post, o tipo de headers e body devem ser checados na documentação da api, pode existir outros atriutos obrigatórios dependendo  da api
   */

    print(_response.statusCode); //statusCode - Retorna o status da operação http
    print(_response.body);
  }

  _put (int id) async { //put - Atuliza todos os campos
    var bodyPost = json.encode({
      "title": "titulo alterado",
      "body": "corpo alterado",
      "userId": 111,
    });

    http.Response _response = await http.put(
      Uri.parse(_urlBase + "/posts/${id.toString()}"),
      headers: {'Content-type': 'application/json; charset=UTF-8',},
      body: bodyPost
    );

  /**
   * http
   *  - .put() - Método para fazer o put, o tipo de headers e body devem ser checados na documentação da api, pode existir outros atriutos obrigatórios dependendo  da api
   */

    print(_response.statusCode);
    print(_response.body);
  }

  _patch (int id) async{ //patch - Atualizar campos esécíficos
    var bodyPost = json.encode({
      // "title": "titulo alterado",
      "body": "corpo alterado",
      "userId": 111,
    });

    http.Response _response = await http.patch(
      Uri.parse(_urlBase + "/posts/${id.toString()}"),
      headers: {'Content-type': 'application/json; charset=UTF-8',},
      body: bodyPost
    );

  /**
   * http
   *  - .patch() - Método para fazer o patch, o tipo de headers e body devem ser checados na documentação da api, pode existir outros atriutos obrigatórios dependendo  da api
   */

    print(_response.statusCode);
    print(_response.body);
  }

  _delete (int id) async{
    http.Response _response = await http.delete(Uri.parse(_urlBase + "/posts/${id.toString()}"));

    /**
     * http
     *  - .delete() - Método para fazer o delete
     */

    print(_response.statusCode);
    print(_response.body);
  }   
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Future and List')),
      body: Column(children: [
        Row(children: [
          TextButton(
            onPressed: _post,
            child: Text("Post")
          ),
          TextButton(
            onPressed: () => _put(2),
            child: Text("Put")
          ),
          TextButton(
            onPressed: () => _patch(2),
            child: Text("Patch")
          ),
          TextButton(
            onPressed: () => _delete(2),
            child: Text("Delete")
          ),
        ],),
        Expanded(child: FutureBuilder<List<Post>>( //Expand - Widget utilizada para expandir um elemento específico dentro da coluna ou linha, só irá expandir se houver espaço
        future: _recuperarPostagens(),
        builder: (context, snapshot){
          switch( snapshot.connectionState ){
            case ConnectionState.none:
            case ConnectionState.waiting:
              Center(child: CircularProgressIndicator());
              break;
            case ConnectionState.active:
            case ConnectionState.done:
              if( snapshot.hasError ){
                print("Erro ao carregar os dados.");
              }else {
                return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index){
                      return ListTile(
                        title: Text(snapshot.data![index].id.toString()),
                        subtitle: Text(snapshot.data![index].title.toString())
                      );
                  }
                );
              }
              break;
          }
          return Container();
        }
      ))
      ],)
    );

  }
}
