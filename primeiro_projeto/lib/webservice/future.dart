import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';


void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Future<Map> _recuperarPreco() async {
    Uri url = Uri.parse('https://blockchain.info/ticker');
    http.Response response = await http.get(url);
    return json.decode( response.body );
  }

  /**
   * Futere<tipoValorEnviado> - Função que retorna o valores que o widget FutureBuilder precisa receber
   */

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map>(
      future: _recuperarPreco(),
      builder: (context, snapshot){

        String resultado;
        switch( snapshot.connectionState ){
          case ConnectionState.none :
          case ConnectionState.waiting :
            print("conexao waiting");
            resultado = "Carregando...";
            break;
          case ConnectionState.active :
          case ConnectionState.done :
            print("conexao done");
            // if( snapshot.hasError ){
            //   resultado = "Erro ao carregar os dados.";
            // }else {

            //   double valor = snapshot.data!["BRL"]["buy"];
            //   resultado = "Preço do bitcoin: ${valor.toString()} ";

            // }
            double valor = snapshot.data!["BRL"]["buy"];
            resultado = "Preço do bitcoin: ${valor.toString()} ";
            break;
        }
        return Center(
          child: Text( resultado ),
        );

      },
    );
    /**
     * FutureBuilder<tipoValorRecebido> - Widget para manipular requisições web
     *  future - Atributo que recebe a função com os dados do serviceweb
     *  builder - Atributo que recebe a função para manipular os dados, context e snapshot parâmetros obrigatórios
     *    snapshot - Parâmetro com métodos diversos
     *      .connectionState - Retorna o tipo de conexão http
     *      .hasError - Retorna true ou false em caso de ter erro
     *      .data - Atributo com os dados enviados da função Future, necessita do ! para manipular
     *    ConnectionState - Contém os tipos de conexões http
     *      .none
     *      .waiting
     *      .active
     *      .done
     */
  }
}