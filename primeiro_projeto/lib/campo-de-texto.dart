import 'package:flutter/material.dart';

void main(){
  TextEditingController controlador = TextEditingController(); //Classe obrigatória para usar o controlador (controller)
  runApp(MaterialApp(
    title: 'Caixa de texto',
    home: Scaffold(
      appBar: AppBar(
        title: Text('Entrada de dados'),
        backgroundColor: Colors.blue 
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(32),
            child: TextField( 
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Digite um valor"
              ),
              enabled: true,
              maxLength: 5,
              style: TextStyle(color: Colors.green),
              obscureText: false,
              onChanged: (String valorDigitado){print(valorDigitado + 'onchaged');},
              onSubmitted: (String valorDigitado){print(valorDigitado + 'onsubmitted');},
              controller: controlador
            /*
            TextField - Widget de caixa de texto, necessita de no mínimo um esqueleto do app (Material) construído
              keyboardType - Atributo que determina o tipo de teclado que será mostrado para o usuário
                TextInputType - Classe para importar os tipos de dados
                  text, name, url, phone, number...
              decoration - Atributo que controla a decoração do texto
                InputDecoration - Classe para fazer a decoração
                  labelText - Frase que irá ficar no input para sinalizar o que o usuário deve digitar
              enabled - Atributo que habilita ou desabilita o campo de texto
                true - Default, habilitado
                false - desabilitado
              maxLength - Atributo delimita a quatidade de caracteres recebido no campo de texto, recebe um valor numérico
              maxLengthEnforced - Recebe true ou false para ajudar a delimitar a quantidade de caracteres
              style - Atributo igual ao de Text().
              obscureText - Esconde o texto escrito como feito em senhas
                false - Default, mostra os texto
                true - Esconde os texto
              onChanged - Atributo que retorna o valor quando há uma modificação no campo de texto, necessita que um parâmentro seja declarado como string
              onSubmitted - Atributo que retorna o que foi escrito quando o usuário aperta o enter
              controller - Atributo que irá ajudar no momento de fazer o submit dos dados a partir de um botão (RaisedButton, etc...), necessário declarar a classe TextEditingController
            */
          ),),
          TextButton(
            child: Text('Salvar'),
            onPressed: (){
              print(controlador); //Recuperando valor digitado após usar o o butão submite.
            },
            )
          ]
        ),
      ),
  ));
}