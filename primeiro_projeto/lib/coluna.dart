import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Frases do dia - coluna",
    home: Column(children: <Widget>[ //Column - widget para organizar em colunas / children: [] - Sequências de widgtes dentro do widget pai
      Text("texto 1"), //Text - text widget
      Text("texto 2"),
      Text("texto 3")
    ],),
  ));
}