import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false, //Remover a faixa de debug
    title: "Espaçamento",
    home: Container(
      //color:Colors.black, Para funcionar o Colors dentro do decoration-BoxDecoration precisa ter um dos atributos (color ou decoration) com o valor null
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0), /*
      padding - Atributo de controle do espaçamento interno do container, é possível usar o paddin como widget assim como column, row, text ... O padding só será aplicado para os filhos diretos no widget, ou em nada em caso de houver nada.
        EdgeInsets - Classe que permiti chamar os métodos de alteração dos valores
          métodos:
            .all - Recebe valor inteiro para definir todos os lados de uma só vez
            .fromLTRB(left, top, right, bottom) - Recebe valor inteiro para definir todos os lados de forma específica
      */
      margin: EdgeInsets.all(0),/* 
      margin - Atributo para definir a margem
      EdgeInsets - Mesmo conceito do padding
          métodos:
            .all() - Mesmo conceito do padding
            .fromLTRB(left, top, right, bottom) - Mesmo conceito do padding
            */
      decoration: BoxDecoration( /*
      decoration - Parametro para poder definir a decoração do container (border, etc...) 
        BoxDecoration - Classe que permiti chamar os atributos que decorão da caixa
      */
        border: Border.all(width: 3, color: Colors.green) /*
        border - Atributo que controla a borda do container
          Boder - Classe que permiti definir a borda
            métodos:
              .all - controla todos os lados
                atributos:
                  width - Recebe um valor inteiro
                  color - recebe a cor
        */
      ),
      child: Column(
        children: [
          /*Text(
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's",
            textAlign: TextAlign.justify, //textAlign - Atributo de controle do alinhamento do texto
            style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              decoration: TextDecoration.none,
            ),
            )*/
          
          Text("t1"),
          Padding(  //Padding - widget igula ao atributo padding de container, é obrigatório o atributo padding dentro da Classe. O padding só será aplicado para os filhos diretos no widget, ou em nada em caso de houver nada.
            padding: EdgeInsets.all(5),
            child: Text("t2"),
            ),
          Text("t3")
        ],
      ),
    )
  ));
}